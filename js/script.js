
// ДЗ 1
// Використовуючи цикли намалюйте: 
// 1 Порожній прямокутник
// Заповнений 
// 2 Рівносторонній трикутний 
// 3 Прямокутний трикутник 
// 4 Ромб


let x = 10,
	y = 10,
	f = '*'.repeat(10);

for (let i = 0; i < y; ++i) {
	let str = (i == 0 || i == y - 1) ? f : '*' + ("&nbsp; ".repeat(x - 2)) + '*';
	document.write(str + "<br/>");
}

document.write('<hr>')

for (let i = 0; i < 10; i++) {
	for (let b = (10 - i); b > 0; b--) {
		document.write('&nbsp;')
	}
	for (let a = 0; a <= i; a++) {
		document.write('*')
	}
	document.write('<br>')
}

document.write('<hr>')


for (let i = 0; i < 10; i++) {
	for (let a = 0; a <= i; a++) {
		document.write('*')
	}
	document.write('<br>')
}

document.write('<hr>')


let space = 10,
	star = 1,
	line = 14;
for (let i = 0; i <= line; i++) {
	for (let j = 0; j <= space; j++) {
		document.write("&nbsp")
	}
	for (let k = 0; k < star; k++) {
		document.write("*")
	}
	space--;
	star++;
	if (i >= line / 2 && i <= line) {
		star = star - 2;
		space = space + 2;
	}
	document.write("<br>");
}

// ДЗ 2
// Створіть масив styles з елементами «Джаз» та «Блюз».
// Додайте «Рок-н-рол» до кінця.
// Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною
// Видаліть перший елемент масиву та покажіть його.
// Вставте «Реп» та «Реггі» на початок масиву.

// Варіант №1

let styles = ['Джаз', 'Блюз'];
styles.push('Рок-н-рол');
let removed = Math.floor(styles.length / 2)
removed = styles.shift(1);
console.log(removed)
styles.splice(0, 0, 'Реп', 'Реггі')
console.log(styles)



// Варіант №2

let style = ['Джаз', 'Блюз'];
style[2] = 'Рок-н-рол';
style[1] = 'Класика';
let remove = style.splice(0, 1);
console.log(remove)
style.splice(0, 0, 'Реп', 'Реггі')
console.log(style)

